from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime,timedelta
# Create your views here.

def hi(request) :
    return render(request,'stry4/story3.html')

def story3(request) :
    return render(request,'stry4/story3.html')

def story3extra(request) :
    return render(request,'stry4/random.html')

def time(request) :
    currentDT = datetime.now()
    return HttpResponse(str(currentDT + timedelta(hours=7)))

def timeextra(request,waktu) :
    if type(waktu) == "string" :
        waktu = datetime.now() - timedelta(hours=int(waktu) + 7)
        return HttpResponse(str(waktu))
    else :
        waktu = datetime.now() + timedelta(hours=int(waktu) + 7)
        return HttpResponse(str(waktu))