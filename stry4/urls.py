from django.urls import path
from . import views

urlpatterns = [
    path('', views.hi, name = "home-page"),
    path('story3', views.story3, name = "story3-page"),
    path('story3extra', views.story3extra, name = "story 3 extra"),
    path('time',views.time, name = "story-time"),
    path('time/<int:waktu>',views.timeextra,name="extra time"),
    path('time/<str:waktu>',views.timeextra,name="waktu  minus"),
]